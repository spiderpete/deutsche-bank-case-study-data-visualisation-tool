# Project's README

# Team: #

* Saruchi Aggarwal		saru.aggarwal19@gmail.com
* Catriona Harvey		catriona.harvey23@gmail.com
* Anton Kapelyushok		amtonk@gmail.com 
* Aaron Leong			aaronleong32@gmail.com
* Johannes M�ller		mueller.johann192@gmail.com
* Ankita Muwal			ankitamuwal@yahoo.com
* Santhosh Sridharan		santhosh.sridharan@outlook.com
* Peter Stefanov		peter.i.stefanov@gmail.com


# Overview #

We are being asked to develop a web based dashboard to represent trading data.  The schema for the data is shown here
![Schema]("extra/DB Case Study Schema.jpg")

The schema has been updated and you should study this in the [schema file attached.](scripts/db_grad_cs_1917_no_deal_data.sql)

The schema represents the relationship (the deal) between counter-party and an instrument.

This information is to be represented in a useful and meaningful manner.


# Objectives and expectations #

Through he use of the new architecture, we will obtain a better appreciation of

* container technology and how it can be exploited
* the difference between virtualization and containers
* how to achieve a QoS like high availability
* what is clustering
* proxies and load balancers
* clear understanding of architecture tiers
* clear understanding of the difference between tiers/layers in the code and runtime/physical tiers, and the deployment of runtime components to different physical tiers
* working with SQL
* working with JQuery, Angular and d3js
* developing application components using TDD
* developing unit tests
* developing applications using BDD
* working in an Agile manner in an Agile team
* the importance of testing

Good architecture tiering

Clear demonstration of the allocation of behaviour to correct tiers

Clear demonstration of data quality appreciation

Business logic should have unit tests

There should be a clear demonstration of a BDD/TDD approach to solving the problem

All released code will be deployed onto a application server that we have no control over

We will build and test their solutions on their local team machine but milestone sign-offs can only be achieved if the application has been deployed onto central application server

We will have access to a central database server, access is through JDBC calls but can be through any means they deem satisfactory.  Ony code that is in the business logic tier should be making direct calls the database server and this must be deployed onto the application server.  In other words it should reside within the web tier (web pages) and should not be run from the browser using jQuery etc style programming.

No business and database logic should be present in the JSPs

The database is available through a container.  But what if it were not available at periodic times?  Each team should demonstrate their approach to continual working if the database is not present, so what would they do if it were only available at periodic times.  We should think about what framework and techniques We can apply to build and test our application when the database server is not available.
