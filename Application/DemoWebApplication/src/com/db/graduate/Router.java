package com.db.graduate;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.controllers.Controller;
import com.db.graduate.controllers.NotFoundController;

public class Router {
	
	public Router() {
		this.controllers = new HashMap<>();
		this.notFoundController = new NotFoundController();
	}
	
	private NotFoundController notFoundController;
	private Map<String, Controller> controllers;
	
	public void register(String path, Controller controller) {
		controllers.put(path, controller);
	}
	
	public void processRequest(HttpServletRequest req, HttpServletResponse res) 
			throws Exception {
		String requestURI = req.getRequestURI();
		int contextPathLength = req.getServletPath().length() + req.getContextPath().length() + 1;

		String relativePath = "";
		if (requestURI.length() >= contextPathLength) {
			relativePath = req.getRequestURI().substring(contextPathLength);
		}

		boolean matched = false;
		for (String path: controllers.keySet()) {
			if (relativePath.matches(path)) {
				Controller controller = controllers.get(path);
				matched = controller.processRequest(req, res);
				break;
			}
		}

		if (!matched) {
			notFoundController.processRequest(req,  res);
		}

	}
}
