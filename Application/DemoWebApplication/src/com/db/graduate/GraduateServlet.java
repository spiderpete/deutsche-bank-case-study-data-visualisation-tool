package com.db.graduate;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.db.graduate.controllers.*;
import com.db.graduate.dao.*;
import com.db.graduate.dao.impl.*;

@SuppressWarnings("serial")
public class GraduateServlet extends HttpServlet {
	
	private Router router = new Router();
	private AuthenticationService authenticationService;
	private Controller forbiddenController = new UnauthorizedController();
	
	@Resource(name = "jdbc/mydb") //provided by tomcat
	private DataSource dataSource;

	@Override
	public void init() {
		UsersRepository usersRepository = new UsersRepositoryImpl(dataSource);
		DealRepository dealRepository = new DealRepositoryImpl(dataSource);
		InstrumentRepository instrumentRepository = new InstrumentRepositoryImpl(dataSource);
		CounterpartyRepository counterpartyRepository = new CounterpartyRepositoryImpl(dataSource);
		AvailabilityCheckService avs = new AvailabilityCheckService(dataSource);
		
		authenticationService = new AuthenticationServiceImpl(usersRepository);

		router.register("authenticate", new AuthenticationController(authenticationService));
		router.register("check", new CheckController(avs));
		router.register("private", authorized(new ProtectedResource()));
		router.register("deals", authorized(new DealController(dealRepository)));
		router.register("instruments", authorized(new InstrumentController(instrumentRepository)));
		router.register("counterparties", authorized(new CounterpartyController(counterpartyRepository)));
		router.register("req1", authorized(new Req1Controller(instrumentRepository)));
		router.register("req2", authorized(new Req2Controller(new Req2Repo(dataSource))));
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) 
			throws ServletException, IOException {
		try {
			res.setHeader("Access-Control-Allow-Origin", "*");
			res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
			res.setHeader("Access-Control-Allow-Headers", "Authorization");
			router.processRequest(req, res);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private Controller authorized(Controller controller) {
		return new ProtectedMiddleware(authenticationService, controller, forbiddenController);
	}

}
