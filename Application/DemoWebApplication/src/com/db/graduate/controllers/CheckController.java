package com.db.graduate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.AvailabilityCheckService;

public class CheckController implements Controller {

	private AvailabilityCheckService avs;
	public CheckController(AvailabilityCheckService avs) {
		this.avs = avs;
	}
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return ControllerHelper.respondWithJson(res, avs.check());
	}

}
