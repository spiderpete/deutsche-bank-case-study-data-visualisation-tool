package com.db.graduate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProtectedResource implements Controller {
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
			res.getWriter().println("welcome");
			return true;
	}

}
