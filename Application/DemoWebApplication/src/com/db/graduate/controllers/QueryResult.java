package com.db.graduate.controllers;

import java.util.List;

@SuppressWarnings("rawtypes")
public class QueryResult {
	private int total;
	private int count;
	private int offset;
	private List data;
	public QueryResult(int total, int count, int offset, List data) {
		super();
		this.total = total;
		this.count = count;
		this.offset = offset;
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public int getCount() {
		return count;
	}
	public int getOffset() {
		return offset;
	}
	public List getData() {
		return data;
	}
}