package com.db.graduate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.InstrumentRepository;
import com.db.graduate.dao.model.Instrument;

public class InstrumentController implements Controller {

	private InstrumentRepository instrumentRepository;

	public InstrumentController(InstrumentRepository instrumentRepository) {
		this.instrumentRepository = instrumentRepository;
	}
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String offsetString = req.getParameter("offset");
		String limitString = req.getParameter("limit");
		
		int offset = 0;
		try {
			offset = Integer.parseInt(offsetString);
		} catch (Exception e) {
			// ignore
		}
		
		int limit = 100;
		try {
			limit = Integer.parseInt(limitString);
		} catch (Exception e) {
			// ignore
		}
		List<Instrument> instruments = instrumentRepository.all(offset, limit);
		int total = instrumentRepository.total();
		return ControllerHelper.respondWithJson(res, new QueryResult(total, instruments.size(), offset, instruments));
	}
	
}
