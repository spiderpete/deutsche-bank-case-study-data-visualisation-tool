package com.db.graduate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.AuthenticationService;

public class ProtectedMiddleware implements Controller {

	private AuthenticationService authenticationService;
	private Controller allowController;
	private Controller forbideController;
	
	public ProtectedMiddleware(
			AuthenticationService authenticationService, 
			Controller allowController, 
			Controller forbideController
		) {
		this.authenticationService = authenticationService;
		this.allowController = allowController;
		this.forbideController = forbideController;
	}
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String token = req.getParameter("token"); 
		if (authenticationService.isAuthenticated(token)) {
			return this.allowController.processRequest(req, res);
		} else {
			return this.forbideController.processRequest(req, res);
		}
	}
}
