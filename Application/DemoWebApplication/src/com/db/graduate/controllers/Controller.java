package com.db.graduate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Controller {
	boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception;
}
