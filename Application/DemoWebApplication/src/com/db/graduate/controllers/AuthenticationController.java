package com.db.graduate.controllers;

import java.io.IOException;
import java.util.Enumeration;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.AuthenticationService;

class ValidResponse {
	private String user;
	private String token;
	public ValidResponse(String user, String token) {
		super();
		this.user = user;
		this.token = token;
	}
	public String getUser() {
		return user;
	}
	public String getToken() {
		return token;
	}
}

class InvalidResponse {
	private String error = "Invalid user or password";
	public String getError() {
		return error;
	}
}

public class AuthenticationController implements Controller {

	private AuthenticationService authenticationService;

	public AuthenticationController(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		String user = req.getParameter("user");
		String password = req.getParameter("password");
		Enumeration<String> names = req.getParameterNames();
		System.out.println("AuthenticationController::processRequest " + user + " " + password);
		System.out.println(names.hasMoreElements());
		while (names.hasMoreElements()) {
			System.out.println(names.nextElement());
		}
		
		   String test = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		   System.out.println("Test");
		   System.out.println(test);
			try {
			String token = authenticationService.createTokenForUser(user, password);
			if (token == null) {
				res.setStatus(401);
				return ControllerHelper.respondWithJson(res, new InvalidResponse());
			}
			return ControllerHelper.respondWithJson(res, new ValidResponse(user, token));	
		} catch (Exception e) {
			e.printStackTrace();
			res.setStatus(401);
			return ControllerHelper.respondWithJson(res, new InvalidResponse());
		}
	}
}
