package com.db.graduate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UnauthorizedController implements Controller {
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.getWriter().println("Unauthorized");
		res.setStatus(401);
		return true;
	}

}
