package com.db.graduate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.InstrumentRepository;
import com.db.graduate.dao.Req2Repo;
import com.db.graduate.dao.model.Instrument;
import com.db.graduate.dao.model.Req2;

public class Req2Controller implements Controller {

	private Req2Repo repo;

	public Req2Controller(Req2Repo repo) {
		this.repo = repo;
	}
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		List<Req2> result = repo.all();
		return ControllerHelper.respondWithJson(res, result);
	}
	
}
