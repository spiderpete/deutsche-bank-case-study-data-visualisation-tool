package com.db.graduate.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ControllerHelper {
	public static boolean respondWithJson(HttpServletResponse res, Object obj) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return respondWithJson(mapper, res, obj);
	}
	
	public static boolean respondWithJson(ObjectMapper mapper, HttpServletResponse res, Object obj) throws IOException {
		PrintWriter out = res.getWriter();
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		out.write(mapper.writeValueAsString(obj));
		return true;
	}
	
	public static String getParamValueOrNull(HttpServletRequest req, String key) {
		String[] values = req.getParameterMap().get(key);
		if (values != null && values.length > 0) {
			return values[0];
		}
		return null;
	}
}
