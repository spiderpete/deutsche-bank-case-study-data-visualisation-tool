package com.db.graduate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.InstrumentRepository;
import com.db.graduate.dao.model.Req1;

public class Req1Controller implements Controller {
	private InstrumentRepository instrumentRepository;

	public Req1Controller(InstrumentRepository instrumentRepository) {
		this.instrumentRepository = instrumentRepository;
	}
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String type = req.getParameter("type");
		if (type == null) {
			type = "B";
		}
		List<Req1> response = instrumentRepository.req1(type);
		return ControllerHelper.respondWithJson(res, response);
	}

}
