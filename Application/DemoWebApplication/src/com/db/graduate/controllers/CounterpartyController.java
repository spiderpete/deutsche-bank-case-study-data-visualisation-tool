package com.db.graduate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.graduate.dao.CounterpartyRepository;
import com.db.graduate.dao.model.Counterparty;

public class CounterpartyController implements Controller {

	private CounterpartyRepository counterpartyRepository;

	public CounterpartyController(CounterpartyRepository counterpartyRepository) {
		this.counterpartyRepository = counterpartyRepository;
	}
	
	@Override
	public boolean processRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String offsetString = req.getParameter("offset");
		String limitString = req.getParameter("limit");
		
		int offset = 0;
		try {
			offset = Integer.parseInt(offsetString);
		} catch (Exception e) {
			// ignore
		}
		
		int limit = 100;
		try {
			limit = Integer.parseInt(limitString);
		} catch (Exception e) {
			// ignore
		}
		List<Counterparty> conterparties = counterpartyRepository.all(offset, limit);
		int total = counterpartyRepository.total();
		return ControllerHelper.respondWithJson(res, new QueryResult(total, conterparties.size(), offset, conterparties));
	}
	
}
