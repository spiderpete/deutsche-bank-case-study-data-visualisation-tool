package com.db.graduate.dao;

public interface UsersRepository {
	String passwordForUser(String user) throws Exception;
}
