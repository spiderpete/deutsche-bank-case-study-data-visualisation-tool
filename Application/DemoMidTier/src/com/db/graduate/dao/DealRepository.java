package com.db.graduate.dao;

import com.db.graduate.dao.model.Deal;

public interface DealRepository extends GenericRepository<Deal> {
}
