package com.db.graduate.dao;

import com.db.graduate.dao.model.Counterparty;

public interface CounterpartyRepository extends GenericRepository<Counterparty>  {
}
