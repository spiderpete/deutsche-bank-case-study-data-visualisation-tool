package com.db.graduate.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.db.graduate.dao.model.Counterparty;
import com.db.graduate.dao.model.Deal;
import com.db.graduate.dao.model.Instrument;
import com.db.graduate.dao.model.Req1;
import com.db.graduate.dao.model.Req2;

public class Req2Repo {
	private DataSource ds;

	private static final String REQ2 = "Select s.cp,  counterparty_name, counterparty_status, counterparty_date_registered, s.i, instrument_name, s.v - b.v as dif " + 
			"from " + 
			"(select deal_counterparty_id cp, deal_instrument_id i, sum(deal_quantity) as v from deal " + 
			"where deal_type = 'S' " + 
			"group by deal_counterparty_id, deal_instrument_id) as s " + 
			"join " + 
			"(select deal_counterparty_id cp, deal_instrument_id i, sum(deal_quantity) as v from deal " + 
			"where deal_type = 'B' " + 
			"group by deal_counterparty_id, deal_instrument_id) as b " + 
			"on s.cp=b.cp and s.i=b.i " + 
			"join counterparty cpp on s.cp = cpp.counterparty_id " + 
			"join instrument ins on s.i = ins.instrument_id ;";
	
	public Req2Repo(DataSource ds) {
		this.ds = ds;
	}
	
	private static Counterparty counterpartyFromResultSet(ResultSet resultSet) throws Exception {
		int id = resultSet.getInt(1);
		String name = resultSet.getString(2);
		String status = resultSet.getString(3);
		String date = resultSet.getString(4);

		return new Counterparty(id, name, status, date);
	
	}
	
	private static Instrument instrumentFromResultSet(ResultSet resultSet) throws Exception {
		int instrumentId = resultSet.getInt(5);
		String instrumentName = resultSet.getString(6);

		return new Instrument(
				instrumentId,
				instrumentName
			);
	
	}

	public List<Req2> all() throws Exception {
		try (Connection connection = ds.getConnection()) {
			List<Req2> result = new ArrayList<>();
			
			PreparedStatement preparedStatement = connection.prepareStatement(REQ2);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.add(new Req2(counterpartyFromResultSet(resultSet), instrumentFromResultSet(resultSet), resultSet.getDouble(7)));
			}
			
			return result;
		}
	}
}
