package com.db.graduate.dao;

import java.sql.Connection;

import javax.sql.DataSource;

public class AvailabilityCheckService {
	private DataSource ds;
	public AvailabilityCheckService(DataSource ds) {
		this.ds = ds;
	}
	
	public boolean check() {
		try (Connection con = ds.getConnection()) {
			return con.isValid(4);
		} catch (Exception e) {
			return false;
		}
	}
	
}
