package com.db.graduate.dao;

import java.util.List;

import com.db.graduate.dao.model.Instrument;
import com.db.graduate.dao.model.Req1;

public interface InstrumentRepository extends GenericRepository<Instrument> {
	List<Req1> req1(String type) throws Exception;
}
