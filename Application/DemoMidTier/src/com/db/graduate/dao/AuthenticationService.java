package com.db.graduate.dao;

public interface AuthenticationService {
	String createTokenForUser(String user, String password) throws Exception;
	boolean isAuthenticated(String token);
}
