package com.db.graduate.dao;

import java.util.List;

public interface GenericRepository<T> {
	int total() throws Exception;
	List<T> all(int limit, int offset) throws Exception;
}
