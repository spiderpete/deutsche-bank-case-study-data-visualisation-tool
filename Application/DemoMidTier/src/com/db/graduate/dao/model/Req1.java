package com.db.graduate.dao.model;

public  class Req1 {
	private Instrument instrument;
	private double price;
	public Req1(Instrument instrument, double price) {
		super();
		this.instrument = instrument;
		this.price = price;
	}
	public Instrument getInstrument() {
		return instrument;
	}
	public double getPrice() {
		return price;
	}
}