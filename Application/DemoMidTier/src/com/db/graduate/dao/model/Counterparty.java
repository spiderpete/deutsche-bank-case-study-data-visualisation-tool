package com.db.graduate.dao.model;

public class Counterparty {
	private int id;
	private String name;
	private String status;
	private String dateRegistered;
	
	@Override
	public String toString() {
		return "Counterparty [id=" + id + ", name=" + name + ", status=" + status + ", dateRegistered=" + dateRegistered
				+ "]";
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public String getDateRegistered() {
		return dateRegistered;
	}

	public Counterparty(int id, String name, String status, String dateRegistered) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.dateRegistered = dateRegistered;
	}
	
	
}
