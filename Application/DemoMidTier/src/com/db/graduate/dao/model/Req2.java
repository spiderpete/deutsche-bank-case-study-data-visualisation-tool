package com.db.graduate.dao.model;

public class Req2 {
	private Counterparty counterparty;
	private Instrument instrument;
	private double dif;
	public Req2(Counterparty counterparty, Instrument instrument, double dif) {
		super();
		this.counterparty = counterparty;
		this.instrument = instrument;
		this.dif = dif;
	}
	public Counterparty getCounterparty() {
		return counterparty;
	}
	public Instrument getInstrument() {
		return instrument;
	}
	public double getDif() {
		return dif;
	}
}
