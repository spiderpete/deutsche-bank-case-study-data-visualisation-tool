package com.db.graduate.dao.model;

public class Deal {
	private int id;
	private String type;
	private int amount;
	private Counterparty counterparty;
	private Instrument instrument;
	private String time;
	private int quantity;
	public Deal(int id, String type, int amount, Counterparty counterparty, Instrument instrument, String time,
			int quantity) {
		super();
		this.id = id;
		this.type = type;
		this.amount = amount;
		this.counterparty = counterparty;
		this.instrument = instrument;
		this.time = time;
		this.quantity = quantity;
	}
	public int getId() {
		return id;
	}
	public String getType() {
		return type;
	}
	public int getAmount() {
		return amount;
	}
	public Counterparty getCounterparty() {
		return counterparty;
	}
	public Instrument getInstrument() {
		return instrument;
	}
	public String getTime() {
		return time;
	}
	public int getQuantity() {
		return quantity;
	} 
	
}
