package com.db.graduate.dao.impl;

import com.db.graduate.dao.AuthenticationService;
import com.db.graduate.dao.UsersRepository;

public class AuthenticationServiceImpl implements AuthenticationService {

	private UsersRepository usersRepo;
	
	public AuthenticationServiceImpl(UsersRepository usersRepo) {
		this.usersRepo = usersRepo;
	}
	
	private String generateTokenForUser(String user) {
		return "dev";
	}
	
	@Override
	public String createTokenForUser(String user, String password) throws Exception {
		String expectedPassword = usersRepo.passwordForUser(user);
		if (expectedPassword != null && expectedPassword.equals(password)) {
			return generateTokenForUser(user);
		}
		return null;
	}

	@Override
	public boolean isAuthenticated(String token) {
		return "dev".equals(token);
	}

}
