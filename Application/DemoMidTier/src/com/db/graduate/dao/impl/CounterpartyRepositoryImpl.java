package com.db.graduate.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.db.graduate.dao.CounterpartyRepository;
import com.db.graduate.dao.model.Counterparty;

public class CounterpartyRepositoryImpl implements CounterpartyRepository {
	private DataSource ds;

	public CounterpartyRepositoryImpl(DataSource ds) {
		this.ds = ds;
	}

	private static final String SELECT_ALL = 
			"select counterparty_id, counterparty_name, counterparty_status," +
					"counterparty_date_registered from counterparty limit ?, ?;";
	
	private static final String REQ2 = "";

	private static Counterparty counterpartyFromResultSet(ResultSet resultSet) throws Exception {
		int id = resultSet.getInt(1);
		String name = resultSet.getString(2);
		String status = resultSet.getString(3);
		String date = resultSet.getString(4);

		return new Counterparty(id, name, status, date);
	
	}
	
	@Override
	public List<Counterparty> all(int offset, int limit) throws Exception {
		try (Connection connection = ds.getConnection()){
			List<Counterparty> result = new ArrayList<>();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
			preparedStatement.setInt(1, offset);
			preparedStatement.setInt(2, limit);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.add(counterpartyFromResultSet(resultSet));
			}
			return result;
		}
	}

	@Override
	public int total() throws Exception {
		try (Connection connection = ds.getConnection()){
			ResultSet resultSet =  connection.prepareStatement("select count(*) from counterparty").executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
	}

	
	
}
