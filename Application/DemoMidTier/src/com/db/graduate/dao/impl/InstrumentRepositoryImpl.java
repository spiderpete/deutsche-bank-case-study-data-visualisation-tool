package com.db.graduate.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.db.graduate.dao.InstrumentRepository;
import com.db.graduate.dao.model.Instrument;
import com.db.graduate.dao.model.Req1;



public class InstrumentRepositoryImpl implements InstrumentRepository {
	
	private DataSource ds;

	public InstrumentRepositoryImpl(DataSource ds) {
		this.ds = ds;
	}

	private static final String SELECT_ALL = "SELECT instrument_id, instrument_name from instrument LIMIT ?, ?";
	private static final String REQ1 = "select instrument_id, instrument_name, sum(deal_amount * deal_quantity) / sum(deal_quantity) from deal right join instrument i on instrument_id = deal_instrument_id where deal_type = ? group by deal_instrument_id;";	
	private static Instrument instrumentFromResultSet(ResultSet resultSet) throws Exception {
		int instrumentId = resultSet.getInt(1);
		String instrumentName = resultSet.getString(2);

		return new Instrument(
				instrumentId,
				instrumentName
			);
	
	}
	
	@Override
	public List<Req1> req1(String type) throws Exception {
		try (Connection connection = ds.getConnection()) {
			List<Req1> result = new ArrayList<>();
			
			PreparedStatement preparedStatement = connection.prepareStatement(REQ1);
			preparedStatement.setString(1, type);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.add(new Req1(instrumentFromResultSet(resultSet), resultSet.getDouble(3)));
			}
			
			return result;
		}
	}
	
	@Override
	public List<Instrument> all(int offset, int limit) throws Exception {
		try (Connection connection = ds.getConnection()){
			List<Instrument> result = new ArrayList<>();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
			preparedStatement.setInt(1, offset);
			preparedStatement.setInt(2, limit);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.add(instrumentFromResultSet(resultSet));
			}
			return result;
		}
	}

	@Override
	public int total() throws Exception {
		try (Connection connection = ds.getConnection()){
			ResultSet resultSet =  connection.prepareStatement("select count(*) from instrument").executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
	}

	
	
}
