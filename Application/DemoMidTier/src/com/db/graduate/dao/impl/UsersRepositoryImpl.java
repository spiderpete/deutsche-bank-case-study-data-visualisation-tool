package com.db.graduate.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.db.graduate.dao.UsersRepository;

public class UsersRepositoryImpl implements UsersRepository {

	public static final String SELECT_SPECIFIC_USER_PASSWORD = 
			"select user_pwd from users where user_id = ? limit 1";

	private DataSource ds; 
	public UsersRepositoryImpl(DataSource ds) {
		this.ds = ds;
	}

	@Override
	public String passwordForUser(String user) throws SQLException {
		if (user == null) {
			return null;
		}

		try (Connection connection = ds.getConnection()) {
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_SPECIFIC_USER_PASSWORD);
			preparedStatement.setString(1,  user);
			ResultSet result = preparedStatement.executeQuery();
			if (result.next()) {
				return result.getString(1);
			}
			return null;
		}
	}


}
