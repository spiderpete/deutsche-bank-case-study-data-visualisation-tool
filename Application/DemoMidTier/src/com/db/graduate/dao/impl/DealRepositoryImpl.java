package com.db.graduate.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.db.graduate.dao.DealRepository;
import com.db.graduate.dao.model.Counterparty;
import com.db.graduate.dao.model.Deal;
import com.db.graduate.dao.model.Instrument;

public class DealRepositoryImpl implements DealRepository {
	private DataSource ds;

	public DealRepositoryImpl(DataSource ds) {
		this.ds = ds;
	}

	private static final String SELECT_ALL = "SELECT " + 
			"d.deal_id, " +
			"d.deal_counterparty_id, " +
			"d.deal_instrument_id, " +
			"d.deal_amount, " +
			"d.deal_type, " +
			"c.counterparty_name, " +
			"c.counterparty_status, " +
			"c.counterparty_date_registered, " +
			"i.instrument_name, " +
			"d.deal_time, " +
			"d.deal_quantity " +
			"FROM " +
			"deal d " +
			"RIGHT JOIN " +
			"counterparty c ON d.deal_counterparty_id = c.counterparty_id " +
			"RIGHT JOIN " +
			"instrument i ON d.deal_instrument_id = i.instrument_id " +
			"LIMIT ?, ?";

	private static Deal dealFromResultSet(ResultSet resultSet) throws Exception {
		int dealId = resultSet.getInt(1);
		int counterpartyId = resultSet.getInt(2);
		int instrumentId = resultSet.getInt(3);
		int dealAmount = resultSet.getInt(4);
		String dealType = resultSet.getString(5);
		String counterpartyName = resultSet.getString(6);
		String counterpartyStatus = resultSet.getString(7);
		String counterpartyDateRegistered = resultSet.getString(8);
		String instrumentName = resultSet.getString(9);
		String dealTime = resultSet.getString(10);
		int dealQuantity = resultSet.getInt(11);
		
		Counterparty counterparty = new Counterparty(
				counterpartyId, 
				counterpartyName, 
				counterpartyStatus,
				counterpartyDateRegistered
			);
		
		Instrument instrument = new Instrument(
				instrumentId,
				instrumentName
			);
		
		return new Deal(
				dealId,
				dealType,
				dealAmount,
				counterparty,
				instrument,
				dealTime,
				dealQuantity
			);
	}

	@Override
	public List<Deal> all(int offset, int limit) throws Exception {
		try (Connection connection = ds.getConnection()){
			List<Deal> result = new ArrayList<>();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL);
			preparedStatement.setInt(1, offset);
			preparedStatement.setInt(2, limit);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				result.add(dealFromResultSet(resultSet));
			}
			return result;
		}
	}
		
	@Override
	public int total() throws Exception {
		try (Connection connection = ds.getConnection()){
			ResultSet resultSet =  connection.prepareStatement("select count(*) from deal").executeQuery();
			resultSet.next();
			return resultSet.getInt(1);
		}
	}

}
