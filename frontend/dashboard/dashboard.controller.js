﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['VisualisationService', 'AuthenticationService', '$rootScope', '$location', '$routeParams', '$scope'];
    function DashboardController(VisualisationService,  AuthenticationService, $rootScope, $location, $routeParams, $scope) {
        this.itemsByPage = 10;


        var ctrl = this;

        ctrl.toReq1 = () => {
            $location.path('/req1/');
        };
        ctrl.isLoading = true;          
        var token = AuthenticationService.GetToken();
          this.displayed = [];
        
          this.callServer = function callServer(tableState) {
            var pagination = tableState.pagination;

            console.log(tableState);
            
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.
            ctrl.isLoading = true;

            VisualisationService.fetchData(token, start, ctrl.itemsByPage, function (response) {
                tableState.pagination.numberOfPages = response.data.total / ctrl.itemsByPage;
                console.log(tableState.pagination.numberOfPages);
                ctrl.displayed = response.data.data;
                ctrl.isLoading = false;          
            });
          };
    }

})();