(function () {
    'use strict';

    angular
        .module('app')
        .factory('VisualisationService', VisualisationService);

        const baseUrl = 'http://199.101.108.156:28080/dbankwebtier-a1-1/api/';
    VisualisationService.$inject = ['$http'];
    function VisualisationService($http) {

        var service = {};

        service.fetchData = fetchData;
        service.fetchReq1 = fetchReq1;
        service.GetAverageBuySellPriceForEachInstrument = GetAverageBuySellPriceForEachInstrument;

        return service;

        
        function fetchReq1(token, type, cb) {
            $http({
                method: 'GET', 
                url: baseUrl + `req1?token=${token}&type=${type}`,
            }).then(function(res) {
                cb(res.data);
            }).catch((err) => console.log(err));
        }

        function fetchData(token, offset, limit, callback) {

            var response;

            var req = {
                method: 'GET',
                url: baseUrl + `deals?token=${token}&limit=${limit}&offset=${offset}`
            }

            $http(req).then(function(res){
                console.log(res);
                response = {
                    success : true,
                    token : token,
                    data : res.data,
                };
                callback(response);
            }, function(res){
                console.log(res);
                response = {
                    success : false,  //for all statuses other than 200
                    token : token,
                    data : res.data,
                    message : 'Unable to fetch data'
                };
                callback(response);
            }).catch((err) => console.log(err));
        }

        function GetAverageBuySellPriceForEachInstrument(token, type, callback) {
            var response;
            // var baseUrl = 'http://10.12.179.69:8080/DemoWebApplication/api/';
            //for deployment
            var baseUrl = 'http://199.101.108.156:28080/dbankwebtier-a1-1/api/'
            
            var req = {
                method: 'GET',
                url: baseUrl + 'req1?token=' + token + '&type=' + type,
            }

            $http(req).then(function(res){
                console.log(res);
                response = {
                    success : true,
                    token : token,
                    data : res.data,
                };
                callback(response);
            }, function(res){
                console.log(res);
                response = {
                    success : false,  //for all statuses other than 200
                    token : token,
                    data : res.data,
                    message : 'Unable to fetch data'
                };
                callback(response);
            });
        }
    }

})();