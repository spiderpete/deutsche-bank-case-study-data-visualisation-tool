(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$http', 'AuthenticationService'];
    function HomeController($http,AuthenticationService) {
        var vm = this;

        vm.checked = false;

        vm.connect = connect();
        function connect() {
            AuthenticationService.Connect(function (response) {
                if (response.data) {
                    vm.connectionStatus = true;
                    // $location.path('/');
                    
                } else {
                    vm.connectionStatus = false;
                }
                vm.checked = true;
            });
        };


    }
})();