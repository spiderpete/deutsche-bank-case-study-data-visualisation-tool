﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('Req1Controller', Req1Controller);

    Req1Controller.$inject = ['VisualisationService', 'AuthenticationService', '$rootScope', '$location', '$routeParams', '$scope'];
    function Req1Controller(VisualisationService,  AuthenticationService, $rootScope, $location, $routeParams, $scope) {
        this.itemsByPage = 12;


        var ctrl = this;
        var type = "S";
        ctrl.back = () => {
            $location.path('/dashboard/');
        };
        ctrl.onSell = () => {
            type = "S";
            this.callServer();
        };

        ctrl.onBuy = () => {
            type = "B";
            console.log('here');
            this.callServer();
        };

        ctrl.isLoading = true;          
        var token = AuthenticationService.GetToken();
          this.displayed = [];
        
          this.callServer = function callServer() {
            ctrl.isLoading = true;

            VisualisationService.fetchReq1(token, type, function (response) {
                ctrl.displayed = response.map((d) => ({
                    name: d.instrument.name,
                    id: d.instrument.id,
                    price: d.price,
                }));
                console.log(ctrl.displayed);
                ctrl.isLoading = false;          
            });
          };
    }

})();